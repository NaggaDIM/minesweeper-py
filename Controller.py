#!/usr/bin/python3
# -*- coding: utf-8 -*-
from random import choice, random


class GameController():

	FIELD_SIZE = 20
	FIELD_DOT_BOMB 	= 'B'
	FIELD_DOT_EMPTY = 'E'

	MASK_DOT_LOCK = 'L'
	MASK_DOT_FLAG = 'F'


	def __init__(self):
		self.game_field = None
		self.game_mask 	= self.generate_game_mask()
		self.game_end = False
		self.game_end_is_win = False
		self.game_end_pos = None
		self.generate_game_field()
		self.max_flag_count = self.get_bomb_count()

	def generate_game_field(self):
		self.game_field = []
		for x in range(self.FIELD_SIZE):
			line = []
			for y in range(self.FIELD_SIZE):
				elem = choice([self.FIELD_DOT_BOMB, self.FIELD_DOT_EMPTY])
				if elem == self.FIELD_DOT_BOMB and random() < 0.7:
					elem = self.FIELD_DOT_EMPTY
				line.append(elem)
			self.game_field.append(line)

		for x in range(self.FIELD_SIZE):
			for y in range(self.FIELD_SIZE):
				if self.game_field[x][y] != self.FIELD_DOT_BOMB:
					self.game_field[x][y] = self.get_nearby_bomb_number([x, y])

	def get_bomb_count(self):
		count = 0
		for x in range(self.FIELD_SIZE):
			for y in range(self.FIELD_SIZE):
				if self.game_field[x][y] == self.FIELD_DOT_BOMB: count+=1
		return count

	def get_flag_count(self):
		count = 0
		for x in range(self.FIELD_SIZE):
			for y in range(self.FIELD_SIZE):
				if self.game_mask[x][y] == self.MASK_DOT_FLAG: count+=1
		return count

	def get_nearby_bomb_number(self, pos):
		bomb_count = 0
		start_x = pos[0] - 1
		start_y = pos[1] - 1
		if start_x >= 0: 
			try:
				if self.game_field[start_x][start_y + 1] == self.FIELD_DOT_BOMB: bomb_count += 1
			except Exception as e: pass
			try:
				if self.game_field[start_x][start_y + 2] == self.FIELD_DOT_BOMB: bomb_count += 1
			except Exception as e: pass

		if start_y >= 0:
			try:
				if self.game_field[start_x + 1][start_y] == self.FIELD_DOT_BOMB: bomb_count += 1
			except Exception as e: pass
			try:
				if self.game_field[start_x + 2][start_y] == self.FIELD_DOT_BOMB: bomb_count += 1
			except Exception as e: pass

		if start_x >= 0 and start_y >= 0:
			try:
				if self.game_field[start_x][start_y] == self.FIELD_DOT_BOMB: bomb_count += 1
			except Exception as e: pass

		try:
			if self.game_field[start_x + 1][start_y + 2] == self.FIELD_DOT_BOMB: bomb_count += 1
		except Exception as e: pass

		try:
			if self.game_field[start_x + 2][start_y + 1] == self.FIELD_DOT_BOMB: bomb_count += 1
		except Exception as e: pass

		try:
			if self.game_field[start_x + 2][start_y + 2] == self.FIELD_DOT_BOMB: bomb_count += 1
		except Exception as e: pass

		return str(bomb_count) if bomb_count > 0 else self.FIELD_DOT_EMPTY

	def generate_game_mask(self):
		result = []
		for x in range(self.FIELD_SIZE):
			temp = []
			for y in range(self.FIELD_SIZE):
				temp.append(self.MASK_DOT_LOCK)
			result.append(temp)
		return result

	def open_mask_cell(self, pos):
		if not self.game_end:
			if self.game_mask[pos[0]][pos[1]] == self.MASK_DOT_LOCK: 
				self.game_mask[pos[0]][pos[1]] = ''
				if self.game_field[pos[0]][pos[1]] == self.FIELD_DOT_BOMB: 
					self.game_end = True
					self.game_end_is_win = False
					self.game_end_pos = pos
				if self.game_field[pos[0]][pos[1]] == self.FIELD_DOT_EMPTY:
					self.open_empty(pos)

				self.validate_game()

	def open_empty(self, pos):
		start_x = pos[0] - 1
		start_y = pos[1] - 1
		if start_x >= 0: 
			try:
				if self.game_field[start_x][start_y + 1] == self.FIELD_DOT_EMPTY and self.game_mask[start_x][start_y + 1] == self.MASK_DOT_LOCK:
					self.game_mask[start_x][start_y + 1] = ''
					self.open_empty([start_x, start_y + 1])
				elif self.game_mask[start_x][start_y + 1] == self.MASK_DOT_LOCK: self.game_mask[start_x][start_y + 1] = ''
			except Exception as e: pass

			try:
				if self.game_field[start_x][start_y + 2] == self.FIELD_DOT_EMPTY and self.game_mask[start_x][start_y + 2] == self.MASK_DOT_LOCK:
					self.game_mask[start_x][start_y + 2] = ''
					self.open_empty([start_x, start_y + 2])
				elif self.game_mask[start_x][start_y + 2] == self.MASK_DOT_LOCK: self.game_mask[start_x][start_y + 2] = ''

			except Exception as e: pass

		if start_y >= 0:
			try:
				if self.game_field[start_x + 1][start_y] == self.FIELD_DOT_EMPTY and self.game_mask[start_x + 1][start_y] == self.MASK_DOT_LOCK:
					self.game_mask[start_x + 1][start_y] = ''
					self.open_empty([start_x + 1, start_y])
				elif self.game_mask[start_x + 1][start_y] == self.MASK_DOT_LOCK: self.game_mask[start_x + 1][start_y] = ''
			except Exception as e: pass

			try:
				if self.game_field[start_x + 2][start_y] == self.FIELD_DOT_EMPTY and self.game_mask[start_x + 2][start_y] == self.MASK_DOT_LOCK:
					self.game_mask[start_x + 2][start_y] = ''
					self.open_empty([start_x + 2, start_y])
				elif self.game_mask[start_x + 2][start_y] == self.MASK_DOT_LOCK: self.game_mask[start_x + 2][start_y] = ''
			except Exception as e: pass

		if start_x >= 0 and start_y >= 0:
			try:
				if self.game_field[start_x][start_y] == self.FIELD_DOT_EMPTY and self.game_mask[start_x][start_y] == self.MASK_DOT_LOCK:
					self.game_mask[start_x][start_y] = ''
					self.open_empty([start_x, start_y])
				elif self.game_mask[start_x][start_y] == self.MASK_DOT_LOCK: self.game_mask[start_x][start_y] = ''
			except Exception as e: pass

		try:
			if self.game_field[start_x + 1][start_y + 2] == self.FIELD_DOT_EMPTY and self.game_mask[start_x + 1][start_y + 2] == self.MASK_DOT_LOCK:
				self.game_mask[start_x + 1][start_y + 2] = ''
				self.open_empty([start_x + 1, start_y + 2])
			elif self.game_mask[start_x + 1][start_y + 2] == self.MASK_DOT_LOCK: self.game_mask[start_x + 1][start_y + 2] = ''
		except Exception as e: pass

		try:
			if self.game_field[start_x + 2][start_y + 1] == self.FIELD_DOT_EMPTY and self.game_mask[start_x + 2][start_y + 1] == self.MASK_DOT_LOCK:
				self.game_mask[start_x + 2][start_y + 1] = ''
				self.open_empty([start_x + 2, start_y + 1])
			elif self.game_mask[start_x + 2][start_y + 1] == self.MASK_DOT_LOCK: self.game_mask[start_x + 2][start_y + 1] = ''
		except Exception as e: pass

		try:
			if self.game_field[start_x + 2][start_y + 2] == self.FIELD_DOT_EMPTY and self.game_mask[start_x + 2][start_y + 2] == self.MASK_DOT_LOCK:
				self.game_mask[start_x + 2][start_y + 2] = ''
				self.open_empty([start_x + 2, start_y + 2])
			elif self.game_mask[start_x + 2][start_y + 2] == self.MASK_DOT_LOCK: self.game_mask[start_x + 2][start_y + 2] = ''
		except Exception as e: pass

	def change_flag_state(self, pos):
		if not self.game_end:
			if self.game_mask[pos[0]][pos[1]] == self.MASK_DOT_LOCK and self.get_flag_count() < self.max_flag_count: self.game_mask[pos[0]][pos[1]] = self.MASK_DOT_FLAG
			elif self.game_mask[pos[0]][pos[1]] == self.MASK_DOT_FLAG: self.game_mask[pos[0]][pos[1]] = self.MASK_DOT_LOCK
			self.validate_game()

	def validate_game(self):
		for x in range(self.FIELD_SIZE):
			for y in range(self.FIELD_SIZE):
				if self.game_field[x][y] == self.FIELD_DOT_BOMB and self.game_mask[x][y] != self.MASK_DOT_FLAG: return
		self.game_end = True
		self.game_end_is_win = True
		self.game_end_pos = None

	def new_game(self):
		self.game_field = None
		self.game_mask 	= self.generate_game_mask()
		self.game_end = False
		self.game_end_is_win = False
		self.game_end_pos = None
		self.generate_game_field()
		self.max_flag_count = self.get_bomb_count()