#!/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QPushButton, QLabel, QMessageBox
from PyQt5.QtGui import QPainter, QImage, QIcon
from PyQt5.QtCore import pyqtSlot
from Controller import GameController
import resources


class MinesweeperUI(QWidget):
	FIELD_ITEM_SIZE = 40
	GAMEFIELD_SIZE = GameController.FIELD_SIZE * FIELD_ITEM_SIZE
	MENU_WIDTH = 300
	MENU_ITEM_X = GAMEFIELD_SIZE + 5
	MENU_ITEM_WIDTH = MENU_WIDTH - 10
	MENU_ITEM_HEIGHT = 30

	WINDOW_WIDTH = GAMEFIELD_SIZE + MENU_WIDTH
	WINDOW_HEIGHT = GAMEFIELD_SIZE
	WINDOW_TITLE = 'MinesweeperPy by NaggaDIM'

	def __init__(self):
		super().__init__()
		self.controller = GameController()
		self.flag_label = QLabel(self)
		self.init_ui()

	def init_ui(self):
		self.setGeometry(200, 80, self.WINDOW_WIDTH, self.WINDOW_HEIGHT)
		self.setFixedSize(self.WINDOW_WIDTH, self.WINDOW_HEIGHT)
		self.setWindowTitle(self.WINDOW_TITLE)

		new_game_btn = QPushButton('Новая Игра', self)
		new_game_btn.setGeometry(
			self.MENU_ITEM_X,
			5,
			self.MENU_ITEM_WIDTH,
			self.MENU_ITEM_HEIGHT
		)
		new_game_btn.clicked.connect(self.new_game)

		rules_btn = QPushButton('Правила', self)
		rules_btn.setGeometry(
			self.MENU_ITEM_X,
			self.WINDOW_HEIGHT - 70,
			self.MENU_ITEM_WIDTH,
			self.MENU_ITEM_HEIGHT
		)
		rules_btn.clicked.connect(self.view_rules)

		about_btn = QPushButton('О разработчике', self)
		about_btn.setGeometry(
			self.MENU_ITEM_X,
			self.WINDOW_HEIGHT - 35,
			self.MENU_ITEM_WIDTH,
			self.MENU_ITEM_HEIGHT
		)
		about_btn.clicked.connect(self.view_about)

		self.flag_label.setGeometry(
			self.MENU_ITEM_X,
			40,
			self.MENU_ITEM_WIDTH,
			self.MENU_ITEM_HEIGHT
		)
		self.flag_label.setText('Флагов осталось: {}'.format(self.controller.max_flag_count - self.controller.get_flag_count()))

		self.mousePressEvent = self.mouse_event

		self.setWindowIcon(QIcon('app_icon.ico'))
		self.show()

	def paintEvent(self, event):
		painter = QPainter(self)
		self.paint_game_field(painter)
		if not self.controller.game_end: self.paint_game_mask(painter)
		else:
			if self.controller.game_end_is_win:
				self.paint_done_positions(painter)
			else: self.paint_lose_position(painter)
		self.update_flag_label()

	def paint_game_field(self, painter):
		for x in range(self.controller.FIELD_SIZE):
			for y in range(self.controller.FIELD_SIZE):
				painter.drawImage(x * self.FIELD_ITEM_SIZE, y * self.FIELD_ITEM_SIZE, self.get_resource_for_dot(self.controller.game_field[y][x]))

	def paint_game_mask(self, painter):
		for x in range(self.controller.FIELD_SIZE):
			for y in range(self.controller.FIELD_SIZE):
				if self.controller.game_mask[y][x] != '': painter.drawImage(x * self.FIELD_ITEM_SIZE, y * self.FIELD_ITEM_SIZE, self.get_resource_for_dot(self.controller.game_mask[y][x]))

	def paint_lose_position(self, painter):
		painter.drawImage(self.controller.game_end_pos[1] * self.FIELD_ITEM_SIZE, self.controller.game_end_pos[0] * self.FIELD_ITEM_SIZE, QImage(':game_field/bomb_lose.png'))

	def paint_done_positions(self, painter):
		for x in range(self.controller.FIELD_SIZE):
			for y in range(self.controller.FIELD_SIZE):
				if self.controller.game_field[y][x] == self.controller.FIELD_DOT_BOMB:
					painter.drawImage(x * self.FIELD_ITEM_SIZE, y * self.FIELD_ITEM_SIZE, QImage(':game_field/bomb_done.png'))

	def get_resource_for_dot(self, dot):
		if dot == self.controller.FIELD_DOT_BOMB: dot = 'bomb'
		if dot == self.controller.FIELD_DOT_EMPTY: dot = 'empty'
		if dot == self.controller.MASK_DOT_LOCK: dot = 'lock'
		if dot == self.controller.MASK_DOT_FLAG: dot = 'flag'
		return QImage(':game_field/{}.png'.format(dot))

	def get_coordinates(self, event):
		return [
			int(event.pos().y() / self.FIELD_ITEM_SIZE),
			int(event.pos().x() / self.FIELD_ITEM_SIZE),
		]

	def mouse_event(self, event):
		if event.pos().x() < self.GAMEFIELD_SIZE and event.pos().y() < self.GAMEFIELD_SIZE:
			coord = self.get_coordinates(event)
			if event.button() == Qt.LeftButton:
				self.controller.open_mask_cell(coord)
			elif event.button() == Qt.RightButton:
				self.controller.change_flag_state(coord)
		self.update()

	@pyqtSlot()
	def new_game(self):
		self.controller.new_game()
		self.update()

	@pyqtSlot()
	def view_rules(self):
		QMessageBox().information(self, "Правила", "1. {}\n2. {}\n3. {}\n4. {}\n".format(
			'Число в ячейке показывает, сколько мин скрыто вокруг данной ячейки . Это число поможет понять вам, где находятся безопасные ячейки, а где находятся бомбы',
			'Если вы открыли ячейку с миной, то игра проиграна',
			'Что бы пометить ячейку, в которой находится бомба, нажмите её правой кнопкой мыши',
			'Игра продолжается до тех пор, пока вы не пометите все заминированые ячейки флажком',
		))

	@pyqtSlot()
	def view_about(self):
		QMessageBox().information(self,
				"О разработчике", 
				"Игру разработал:\n\tСмертин Дмитрий Анатольевич (NaggaDIM)\n\nСайт разработчика: http://naggadim.ru\n"
		)

	def update_flag_label(self):
		self.flag_label.setText('Флагов осталось: {}'.format(self.controller.max_flag_count - self.controller.get_flag_count()))
